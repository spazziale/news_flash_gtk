# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the newsflash package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: newsflash\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-10 12:18+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:7
#: data/io.gitlab.news_flash.NewsFlash.desktop.in.in:3
#: data/resources/ui_templates/main_window.blp:6
#: data/resources/ui_templates/main_window.blp:14
#: data/resources/ui_templates/main_window.blp:42
msgid "NewsFlash"
msgstr ""

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:8
msgid "Keep up with your feeds"
msgstr ""

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:12
msgid ""
"NewsFlash is a program designed to complement an already existing web-based "
"RSS reader account."
msgstr ""

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:16
msgid ""
"It combines all the advantages of web based services like syncing across all "
"your devices with everything you expect from a modern desktop program: "
"Desktop notifications, fast search and filtering, tagging, handy keyboard "
"shortcuts and having access to all your articles for as long as you like."
msgstr ""

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:55
msgid "Fix small UI paper cuts"
msgstr ""

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:56
msgid "Local RSS: don't discard failing feeds"
msgstr ""

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:57
msgid "Fix autostart"
msgstr ""

#: data/io.gitlab.news_flash.NewsFlash.appdata.xml.in.in:540
msgid "Jan Lukas Gernert"
msgstr ""

#: data/io.gitlab.news_flash.NewsFlash.desktop.in.in:4
msgid "RSS Reader"
msgstr ""

#: data/io.gitlab.news_flash.NewsFlash.desktop.in.in:5
msgid ""
"Faster than flash to deliver you the latest news from your subscribed feeds"
msgstr ""

#: data/io.gitlab.news_flash.NewsFlash.desktop.in.in:11
msgid "Gnome;GTK;RSS;Feed;"
msgstr ""

#: data/resources/ui_templates/add_dialog/category.blp:39
msgid "Add a Category"
msgstr ""

#: data/resources/ui_templates/add_dialog/category.blp:48
msgid "Enter the Title of the new Category"
msgstr ""

#: data/resources/ui_templates/add_dialog/category.blp:57
msgid "Category Title"
msgstr ""

#: data/resources/ui_templates/add_dialog/category.blp:63
#: data/resources/ui_templates/add_dialog/feed_widget.blp:71
#: data/resources/ui_templates/add_dialog/tag.blp:62
msgid "Add"
msgstr ""

#: data/resources/ui_templates/add_dialog/error.blp:31
msgid "Try again"
msgstr ""

#: data/resources/ui_templates/add_dialog/feed.blp:14
msgid "Parse URL"
msgstr ""

#: data/resources/ui_templates/add_dialog/feed.blp:20
#: data/resources/ui_templates/discover/dialog.blp:314
msgid "Select Feed"
msgstr ""

#: data/resources/ui_templates/add_dialog/feed.blp:26
#: data/resources/ui_templates/add_dialog/feed_widget.blp:33
#: data/resources/ui_templates/discover/dialog.blp:320
msgid "Add Feed"
msgstr ""

#: data/resources/ui_templates/add_dialog/feed.blp:32
msgid "Error"
msgstr ""

#: data/resources/ui_templates/add_dialog/feed_widget.blp:42
msgid "Edit the title of the feed and set its category"
msgstr ""

#: data/resources/ui_templates/add_dialog/feed_widget.blp:62
#: data/resources/ui_templates/sidebar/feedlist_dnd_icon.blp:29
#: data/resources/ui_templates/sidebar/feedlist_item.blp:29
msgid "Feed Title"
msgstr ""

#: data/resources/ui_templates/add_dialog/feed_widget.blp:65
#: data/resources/ui_templates/edit_dialogs/feed.blp:50
#: src/content_page/sidebar_column.rs:147
msgid "Category"
msgstr ""

#: data/resources/ui_templates/add_dialog/parse_feed.blp:35
msgid "Add a Feed"
msgstr ""

#: data/resources/ui_templates/add_dialog/parse_feed.blp:44
msgid "Enter the URL of a feed or website"
msgstr ""

#: data/resources/ui_templates/add_dialog/parse_feed.blp:53
msgid "Website or Feed URL"
msgstr ""

#: data/resources/ui_templates/add_dialog/parse_feed.blp:60
msgid "Parse"
msgstr ""

#: data/resources/ui_templates/add_dialog/select_feed.blp:28
msgid "Multiple Feeds found"
msgstr ""

#: data/resources/ui_templates/add_dialog/select_feed.blp:37
msgid "Select one of the listed feeds below"
msgstr ""

#: data/resources/ui_templates/add_dialog/select_feed.blp:61
msgid "Select"
msgstr ""

#: data/resources/ui_templates/add_dialog/tag.blp:37
msgid "Add a Tag"
msgstr ""

#: data/resources/ui_templates/add_dialog/tag.blp:46
msgid "Enter the Title of the new Tag"
msgstr ""

#: data/resources/ui_templates/add_dialog/tag.blp:56
#: data/resources/ui_templates/sidebar/tag_row.blp:25
#: data/resources/ui_templates/tagging/row.blp:26
msgid "Tag Title"
msgstr ""

#: data/resources/ui_templates/article_list/column.blp:27
msgid "Refresh Content"
msgstr ""

#: data/resources/ui_templates/article_list/column.blp:44
msgid "Search Articles"
msgstr ""

#: data/resources/ui_templates/article_list/column.blp:57
msgid "Set All/Feed/Category as Read"
msgstr ""

#: data/resources/ui_templates/article_list/column.blp:94
msgid "All"
msgstr ""

#: data/resources/ui_templates/article_list/column.blp:101
msgid "Unread"
msgstr ""

#: data/resources/ui_templates/article_list/column.blp:108
msgid "Starred"
msgstr ""

#: data/resources/ui_templates/article_list/list.blp:19
msgid "Select an article to read it."
msgstr ""

#: data/resources/ui_templates/article_list/row.blp:38
msgid "feed"
msgstr ""

#: data/resources/ui_templates/article_list/row.blp:53
msgid "/"
msgstr ""

#: data/resources/ui_templates/article_list/row.blp:67
msgid "date"
msgstr ""

#: data/resources/ui_templates/article_list/row.blp:95
#: data/resources/ui_templates/article_list/row.blp:96
msgid "title"
msgstr ""

#: data/resources/ui_templates/article_list/row.blp:110
msgid "No Summary"
msgstr ""

#: data/resources/ui_templates/article_view/column.blp:21
#: data/resources/ui_templates/settings/shortcuts.blp:63
msgid "Toggle Starred"
msgstr ""

#: data/resources/ui_templates/article_view/column.blp:29
msgid "unmarked"
msgstr ""

#: data/resources/ui_templates/article_view/column.blp:37
msgid "marked"
msgstr ""

#: data/resources/ui_templates/article_view/column.blp:52
#: data/resources/ui_templates/settings/shortcuts.blp:46
#: src/article_list/article_row.rs:388
msgid "Toggle Read"
msgstr ""

#: data/resources/ui_templates/article_view/column.blp:60
msgid "read"
msgstr ""

#: data/resources/ui_templates/article_view/column.blp:68
msgid "unread"
msgstr ""

#: data/resources/ui_templates/article_view/column.blp:82
msgid "Article Menu"
msgstr ""

#: data/resources/ui_templates/article_view/column.blp:120
#: data/resources/ui_templates/article_view/column.blp:185
msgid "Try to Show Full Content"
msgstr ""

#: data/resources/ui_templates/article_view/column.blp:140
#: data/resources/ui_templates/article_view/column.blp:204
msgid "Tag Article"
msgstr ""

#: data/resources/ui_templates/article_view/column.blp:152
#: data/resources/ui_templates/article_view/column.blp:216
msgid "Share To"
msgstr ""

#: data/resources/ui_templates/article_view/url.blp:14
#: data/resources/ui_templates/settings/share.blp:119
msgid "URL"
msgstr ""

#: data/resources/ui_templates/article_view/view.blp:35
msgid "crash"
msgstr ""

#: data/resources/ui_templates/article_view/view.blp:47
msgid "WebKit has crashed"
msgstr ""

#: data/resources/ui_templates/article_view/view.blp:65
msgid "empty"
msgstr ""

#: data/resources/ui_templates/article_view/view.blp:68
msgid "Select an article"
msgstr ""

#: data/resources/ui_templates/article_view/view.blp:78
msgid "article"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:15
#: data/resources/ui_templates/discover/dialog.blp:25
msgid "Discover"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:26
msgid "Search the feedly.com library"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:62
msgid "Search by #topic, Website or RSS Link"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:83
msgid "page0"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:91
msgid "Featured Topics"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:112
msgid "#News"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:124
msgid "#Tech"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:134
msgid "#Science"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:144
msgid "#Culture"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:156
msgid "#Media"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:168
msgid "#Sports"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:180
msgid "#Food"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:192
msgid "#Open source"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:205
msgid "page1"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:215
msgid "Search Results"
msgstr ""

#: data/resources/ui_templates/discover/dialog.blp:279
msgid "No Results"
msgstr ""

#: data/resources/ui_templates/discover/search_item.blp:31
msgid "Title"
msgstr ""

#: data/resources/ui_templates/discover/search_item.blp:42
msgid "Description"
msgstr ""

#: data/resources/ui_templates/edit_dialogs/category.blp:18
msgid "Edit Category"
msgstr ""

#: data/resources/ui_templates/edit_dialogs/category.blp:38
msgid "Category Name"
msgstr ""

#: data/resources/ui_templates/edit_dialogs/feed.blp:19
msgid "Edit Feed"
msgstr ""

#: data/resources/ui_templates/edit_dialogs/feed.blp:39
msgid "Feed Name"
msgstr ""

#: data/resources/ui_templates/edit_dialogs/feed.blp:44
msgid "Feed URL"
msgstr ""

#: data/resources/ui_templates/edit_dialogs/feed.blp:55
msgid "Content"
msgstr ""

#: data/resources/ui_templates/edit_dialogs/feed.blp:59
msgid "Inline Math delimiter"
msgstr ""

#: data/resources/ui_templates/edit_dialogs/tag.blp:15
msgid "Edit Tag"
msgstr ""

#: data/resources/ui_templates/edit_dialogs/tag.blp:34
msgid "Tag Name"
msgstr ""

#: data/resources/ui_templates/edit_dialogs/tag.blp:39
msgid "Color"
msgstr ""

#: data/resources/ui_templates/enclosures/button.blp:12
#: src/enclosure_button.rs:90 src/enclosure_button.rs:95
msgid "Open Attachment"
msgstr ""

#: data/resources/ui_templates/enclosures/button.blp:23
msgid "Show Attachments"
msgstr ""

#: data/resources/ui_templates/error_detail_dialog.blp:16
msgid "Error Details"
msgstr ""

#: data/resources/ui_templates/error_detail_dialog.blp:36
msgid "Copy to Clipboard"
msgstr ""

#: data/resources/ui_templates/login/custom_api_secret.blp:25
msgid "Custom API Secret"
msgstr ""

#: data/resources/ui_templates/login/custom_api_secret.blp:50
msgid "Client ID"
msgstr ""

#: data/resources/ui_templates/login/custom_api_secret.blp:54
msgid "Client Secret"
msgstr ""

#: data/resources/ui_templates/login/custom_api_secret.blp:68
msgid "Use Your Own Secret"
msgstr ""

#: data/resources/ui_templates/login/custom_api_secret.blp:81
msgid "Use NewsFlash Secret"
msgstr ""

#: data/resources/ui_templates/login/password.blp:32
msgid "Headline"
msgstr ""

#: data/resources/ui_templates/login/password.blp:51
msgid "Server URL"
msgstr ""

#: data/resources/ui_templates/login/password.blp:55
msgid "Login with"
msgstr ""

#: data/resources/ui_templates/login/password.blp:57
msgid "Username / Password"
msgstr ""

#: data/resources/ui_templates/login/password.blp:57
#: data/resources/ui_templates/login/password.blp:70
msgid "Token"
msgstr ""

#: data/resources/ui_templates/login/password.blp:62
#: data/resources/ui_templates/login/password.blp:85
msgid "Username"
msgstr ""

#: data/resources/ui_templates/login/password.blp:66
#: data/resources/ui_templates/login/password.blp:89
msgid "Password"
msgstr ""

#: data/resources/ui_templates/login/password.blp:82
msgid "HTTP Basic Auth"
msgstr ""

#: data/resources/ui_templates/login/password.blp:108
#: data/resources/ui_templates/main_window.blp:21
#: data/resources/ui_templates/main_window.blp:28
#: data/resources/ui_templates/main_window.blp:35
msgid "Log In"
msgstr ""

#: data/resources/ui_templates/login/service_row.blp:5
msgid "Service Name"
msgstr ""

#: data/resources/ui_templates/login/welcome.blp:16
msgid "Add RSS Service"
msgstr ""

#: data/resources/ui_templates/login/welcome.blp:27
msgid "This Device"
msgstr ""

#: data/resources/ui_templates/login/welcome.blp:39
msgid "Sync Account"
msgstr ""

#: data/resources/ui_templates/main_window.blp:50
msgid "Reset"
msgstr ""

#: data/resources/ui_templates/reset_page.blp:15
msgid "Logout?"
msgstr ""

#: data/resources/ui_templates/reset_page.blp:16
msgid "Logging out leads to all local data being lost"
msgstr ""

#: data/resources/ui_templates/reset_page.blp:31 src/account_popover.rs:22
msgid "Logout"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:6
msgid "App"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:9
msgid "Application"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:12
msgid "Keep running"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:13
msgid "Fetch updates in the background"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:24
msgid "Autostart"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:25
msgid "Start App on login"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:36
msgid "Sync on startup"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:37
msgid "Fetch updates when the App is launched"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:48
msgid "Sync on metered connection"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:49
msgid "Fetch updates on mobile connections in the background"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:61
msgid "Update Interval"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:64 src/settings/feed_list.rs:32
msgid "Manual"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:65
msgid "No automatic synchronization"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:75
msgid "Synchronize every"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:86
msgid "Custom Interval"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:96
msgid "hh:mm:ss"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:107
msgid "Data"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:110
msgid "User Data"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:127
msgid "Keep Articles"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:128
msgid "Refers to the time of sync"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:133
msgid "Cache"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:150
msgid "Webview"
msgstr ""

#: data/resources/ui_templates/settings/app.blp:158
msgid "Clear"
msgstr ""

#: data/resources/ui_templates/settings/keybind_editor.blp:34
msgid "Enter new keybinding to change"
msgstr ""

#: data/resources/ui_templates/settings/keybind_editor.blp:44
msgid "STUFF"
msgstr ""

#: data/resources/ui_templates/settings/keybind_editor.blp:56
msgid "Press ESC to cancel or Backspace to disable the keybinding."
msgstr ""

#: data/resources/ui_templates/settings/keybind_editor.blp:64
msgid "SHORTCUT"
msgstr ""

#: data/resources/ui_templates/settings/keybind_editor.blp:75
msgid "Set"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:6
msgid "Sharing"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:9
msgid "Services"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:12
msgid "Pocket"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:26
msgid "Instapaper"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:40
msgid "Twitter"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:54
msgid "Mastodon"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:68
msgid "Reddit"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:82
msgid "Telegram"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:97
msgid "Custom"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:98
msgid ""
"Add your own share link. ${url} and ${title} will be replaced with the url "
"and title \trespectively."
msgstr ""

#: data/resources/ui_templates/settings/share.blp:101
msgid "Enabled"
msgstr ""

#: data/resources/ui_templates/settings/share.blp:114
msgid "Name"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:6
msgid "Keybindings"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:9
#: data/resources/ui_templates/settings/views.blp:32
msgid "Article List"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:12
msgid "Next Article"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:29
msgid "Previous Article"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:80
msgid "Open URL"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:97
msgid "Copy URL"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:115
#: data/resources/ui_templates/settings/views.blp:9
msgid "Feed List"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:118
msgid "Next Item"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:135
msgid "Previous Item"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:152
msgid "Expand / Collapse"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:169
msgid "Mark selected read"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:187
msgid "General"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:190
msgid "Shortcuts"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:207
msgid "Refresh"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:224
msgid "Search"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:241
#: src/content_page/sidebar_column.rs:128
msgid "Quit"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:258
#: data/resources/ui_templates/sidebar/sidebar.blp:31
msgid "All Articles"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:275
msgid "Only Unread"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:292
msgid "Only Starred"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:310
#: data/resources/ui_templates/settings/views.blp:70
msgid "Article View"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:313
msgid "Scroll up"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:330
msgid "Scroll down"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:347
msgid "Scrape article content"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:364
msgid "Tag article"
msgstr ""

#: data/resources/ui_templates/settings/shortcuts.blp:381
msgid "Fullscreen article"
msgstr ""

#: data/resources/ui_templates/settings/theme_chooser.blp:28
#: data/resources/ui_templates/settings/views.blp:90
msgid "Default"
msgstr ""

#: data/resources/ui_templates/settings/theme_chooser.blp:42
msgid "Spring"
msgstr ""

#: data/resources/ui_templates/settings/theme_chooser.blp:56
msgid "Midnight"
msgstr ""

#: data/resources/ui_templates/settings/theme_chooser.blp:70
msgid "Parchment"
msgstr ""

#: data/resources/ui_templates/settings/theme_chooser.blp:84
msgid "Gruvbox"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:6
msgid "Views"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:12
#: data/resources/ui_templates/settings/views.blp:35
msgid "Order"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:16
msgid "Only show relevant items"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:17
msgid "Hide feeds and categories without unread/unstarred items"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:39
msgid "Show Thumbnails"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:40
msgid "Only if available"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:54
msgid "Hide Future Articles"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:55
msgid "Hide Articles dated in the Future"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:73
msgid "Theme"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:100
msgid "Allow selection"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:113
msgid "Content Width"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:114
msgid "Width of the Article in Characters"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:127
msgid "Line Height"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:128
msgid "Line Height in Characters"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:142
msgid "Use System Font"
msgstr ""

#: data/resources/ui_templates/settings/views.blp:155
msgid "Font"
msgstr ""

#: data/resources/ui_templates/sidebar/account_widget.blp:19
msgid "label"
msgstr ""

#: data/resources/ui_templates/sidebar/column.blp:13
msgid "Account"
msgstr ""

#: data/resources/ui_templates/sidebar/column.blp:23
msgid "Main Menu"
msgstr ""

#: data/resources/ui_templates/sidebar/column.blp:29
msgid "Add Feed/Category/Tag"
msgstr ""

#: data/resources/ui_templates/sidebar/sidebar.blp:74
msgid "Subscriptions"
msgstr ""

#: data/resources/ui_templates/sidebar/sidebar.blp:115
msgid "Tags"
msgstr ""

#: data/resources/ui_templates/tagging/popover.blp:30
msgid "Add Tags"
msgstr ""

#: data/resources/ui_templates/tagging/popover.blp:38
msgid "Type to create a new Tag"
msgstr ""

#: data/resources/ui_templates/tagging/popover.blp:59
msgid "Create Tag"
msgstr ""

#: data/resources/ui_templates/tagging/popover.blp:96
msgid "Tag the Article"
msgstr ""

#: data/resources/ui_templates/tagging/popover.blp:110
msgid "Press enter to assign a Tag or ctrl + enter to create a new Tag"
msgstr ""

#: data/resources/ui_templates/tagging/popover.blp:147
msgid "Search and Create Tags"
msgstr ""

#: src/account_popover.rs:35
msgid "Edit Account"
msgstr ""

#: src/app.rs:451
msgid "Edit category: no parameter"
msgstr ""

#: src/app.rs:472
msgid "Delete category: no parameter"
msgstr ""

#: src/app.rs:518
msgid "Edit feed: no parameter"
msgstr ""

#: src/app.rs:538
msgid "Delete feed: no parameter"
msgstr ""

#: src/app.rs:589
msgid "Delete Tag: no parameter"
msgstr ""

#: src/app.rs:731
msgid "save webview image: no parameter"
msgstr ""

#: src/app.rs:820
msgid "open uri: no parameter"
msgstr ""

#: src/app.rs:992 src/app.rs:1035
msgid "Failed to sync"
msgstr ""

#: src/app.rs:1049
msgid "New Articles"
msgstr ""

#: src/app.rs:1052
msgid "There is 1 new article ({} unread)"
msgstr ""

#: src/app.rs:1055
msgid "There are {} new articles ({} unread)"
msgstr ""

#: src/app.rs:1236
msgid "Failed to mark article read: '{}'"
msgstr ""

#: src/app.rs:1282
msgid "Failed to star article: '{}'"
msgstr ""

#: src/app.rs:1436 src/app.rs:1464 src/app.rs:1492 src/app.rs:1520
msgid "Failed to mark all read"
msgstr ""

#: src/app.rs:1553
msgid "Failed to add feed"
msgstr ""

#: src/app.rs:1573
msgid "Failed to add category"
msgstr ""

#: src/app.rs:1602
msgid "Failed to add tag"
msgstr ""

#: src/app.rs:1641
msgid "Failed to rename feed"
msgstr ""

#: src/app.rs:1665
msgid "Failed to rename category"
msgstr ""

#: src/app.rs:1690 src/app.rs:1870
msgid "Failed to tag article"
msgstr ""

#: src/app.rs:1713
msgid "Failed to set feed list order to manual"
msgstr ""

#: src/app.rs:1743
msgid "Failed to move feed"
msgstr ""

#: src/app.rs:1771
msgid "Failed to move category"
msgstr ""

#: src/app.rs:1789
msgid "Failed to delete feed"
msgstr ""

#: src/app.rs:1809
msgid "Failed to delete category"
msgstr ""

#: src/app.rs:1829
msgid "Failed to delete tag"
msgstr ""

#: src/app.rs:1908
msgid "Failed to untag article"
msgstr ""

#: src/app.rs:1981 src/app.rs:2255 src/app.rs:2357
msgid "_Save"
msgstr ""

#: src/app.rs:1982 src/content_page/article_view_column.rs:222
msgid "Export Article"
msgstr ""

#: src/app.rs:2007
msgid "No file set: {}"
msgstr ""

#: src/app.rs:2033
msgid "Failed to download images"
msgstr ""

#: src/app.rs:2138
msgid "Scraper failed to extract content"
msgstr ""

#: src/app.rs:2142
msgid "Scraper Timeout"
msgstr ""

#. .transient_for(&App::default().main_window())
#: src/app.rs:2164
msgid "_Open"
msgstr ""

#: src/app.rs:2165 src/content_page/sidebar_column.rs:131
msgid "Import OPML"
msgstr ""

#: src/app.rs:2193
msgid "Failed read OPML string: {}"
msgstr ""

#: src/app.rs:2229
msgid "Failed to import OPML"
msgstr ""

#: src/app.rs:2256 src/content_page/sidebar_column.rs:132
msgid "Export OPML"
msgstr ""

#: src/app.rs:2276
msgid "No file set."
msgstr ""

#: src/app.rs:2299
msgid "Failed to get OPML data"
msgstr ""

#: src/app.rs:2310
msgid "Failed to write OPML data to disc"
msgstr ""

#: src/app.rs:2317
msgid "Failed to parse OPML data for formatting"
msgstr ""

#: src/app.rs:2358
msgid "Save Image"
msgstr ""

#: src/app.rs:2378
msgid "No file set"
msgstr ""

#: src/app.rs:2401
msgid "Failed to download image {}"
msgstr ""

#: src/app.rs:2511
msgid "NewsFlash is offline"
msgstr ""

#: src/app.rs:2513
msgid "NewsFlash is online"
msgstr ""

#: src/app.rs:2528
msgid "Failed to set offline"
msgstr ""

#: src/app.rs:2530
msgid "Failed to apply changes made while offline"
msgstr ""

#: src/app.rs:2556
msgid "Error writing settings"
msgstr ""

#: src/app.rs:2624
msgid "Failed read URL: {}"
msgstr ""

#: src/edit_feed_dialog.rs:93 src/add_dialog/add_feed_widget.rs:142
msgid "None"
msgstr ""

#: src/enclosure_button.rs:84
msgid "Open Attached Image"
msgstr ""

#: src/enclosure_button.rs:86
msgid "Open Attached Video"
msgstr ""

#: src/enclosure_button.rs:88
msgid "Open Attached Audio"
msgstr ""

#: src/enclosure_button.rs:93
msgid "Watch Youtube Video"
msgstr ""

#: src/image_dialog.rs:125 src/image_dialog.rs:162
msgid "Failed to decode image '{}'"
msgstr ""

#: src/image_dialog.rs:138
msgid "Failed to downlaod image"
msgstr ""

#: src/image_dialog.rs:173
msgid "Could not decode image"
msgstr ""

#: src/reset_page.rs:92
msgid "Failed to reset account"
msgstr ""

#: src/reset_page.rs:94 src/content_page/mod.rs:145
#: src/login_screen/password_login.rs:317
#: src/login_screen/password_login.rs:328
#: src/login_screen/password_login.rs:339 src/login_screen/web_login.rs:124
msgid "details"
msgstr ""

#: src/add_dialog/add_feed_widget.rs:116
msgid "Failed to add feed: No valid url"
msgstr ""

#: src/add_dialog/parse_feed_widget.rs:117
msgid "Not a valid URL"
msgstr ""

#: src/add_dialog/parse_feed_widget.rs:152
msgid "No Feed found"
msgstr ""

#: src/add_dialog/select_feed_widget.rs:105
msgid "Can't parse Feed"
msgstr ""

#: src/article_list/article_row.rs:389
msgid "Toggle Star"
msgstr ""

#: src/article_list/article_row.rs:390
#: src/content_page/article_view_column.rs:224
msgid "Open in Browser"
msgstr ""

#: src/article_list/mod.rs:679
msgid "No articles that fit \"{}\""
msgstr ""

#: src/article_list/mod.rs:680
msgid "No articles"
msgstr ""

#: src/article_list/mod.rs:683
msgid "No unread articles that fit \"{}\""
msgstr ""

#: src/article_list/mod.rs:684
msgid "No unread articles"
msgstr ""

#: src/article_list/mod.rs:687
msgid "No starred articles that fit \"{}\""
msgstr ""

#: src/article_list/mod.rs:688
msgid "No starred articles"
msgstr ""

#: src/article_list/mod.rs:699
msgid "No articles that fit \"{}\" in {} \"{}\""
msgstr ""

#: src/article_list/mod.rs:702
msgid "No articles in {} \"{}\""
msgstr ""

#: src/article_list/mod.rs:706
msgid "No unread articles that fit \"{}\" in {} \"{}\""
msgstr ""

#: src/article_list/mod.rs:709
msgid "No unread articles in {} \"{}\""
msgstr ""

#: src/article_list/mod.rs:713
msgid "No starred articles that fit \"{}\" in {} \"{}\""
msgstr ""

#: src/article_list/mod.rs:716
msgid "No starred articles in {} \"{}\""
msgstr ""

#: src/article_list/mod.rs:722
msgid "No articles that fit \"{}\" in tag \"{}\""
msgstr ""

#: src/article_list/mod.rs:723
msgid "No articles in tag \"{}\""
msgstr ""

#: src/article_list/mod.rs:727
msgid "No unread articles that fit \"{}\" in tag \"{}\""
msgstr ""

#: src/article_list/mod.rs:730
msgid "No unread articles in tag \"{}\""
msgstr ""

#: src/article_list/mod.rs:734
msgid "No starred articles that fit \"{}\" in tag \"{}\""
msgstr ""

#: src/article_list/mod.rs:737
msgid "No starred articles in tag \"{}\""
msgstr ""

#: src/content_page/article_view_column.rs:227
msgid "Toggle Fullscreen"
msgstr ""

#: src/content_page/article_view_column.rs:228
msgid "Close Article"
msgstr ""

#: src/content_page/mod.rs:192
msgid "Deleted Category '{}'"
msgstr ""

#: src/content_page/mod.rs:193
msgid "Deleted Feed '{}'"
msgstr ""

#: src/content_page/mod.rs:194
msgid "Deleted Tag '{}'"
msgstr ""

#: src/content_page/mod.rs:198
msgid "undo"
msgstr ""

#: src/content_page/mod.rs:323
msgid "Failed to set service logo"
msgstr ""

#: src/content_page/mod.rs:750
msgid "Sidebar: missing category with id: '{}'"
msgstr ""

#: src/content_page/mod.rs:786
msgid "Sidebar: missing feed with id: '{}'"
msgstr ""

#: src/content_page/mod.rs:850
msgid "Failed to update sidebar: '{}'"
msgstr ""

#: src/content_page/sidebar_column.rs:125
msgid "Preferences"
msgstr ""

#: src/content_page/sidebar_column.rs:126
msgid "Keyboard Shortcuts"
msgstr ""

#: src/content_page/sidebar_column.rs:127
msgid "About NewsFlash"
msgstr ""

#: src/content_page/sidebar_column.rs:135
msgid "Discover Feeds"
msgstr ""

#: src/content_page/sidebar_column.rs:146
msgid "Feed"
msgstr ""

#: src/content_page/sidebar_column.rs:148
msgid "Tag"
msgstr ""

#. set headline
#: src/login_screen/password_login.rs:156
msgid "Log into {}"
msgstr ""

#: src/login_screen/password_login.rs:285
msgid "Unauthorized"
msgstr ""

#: src/login_screen/password_login.rs:295
msgid "No valid CA certificate available"
msgstr ""

#: src/login_screen/password_login.rs:299
msgid "ignore future TLS errors"
msgstr ""

#: src/login_screen/password_login.rs:313
msgid "Network Error"
msgstr ""

#: src/login_screen/password_login.rs:324
msgid "Could not log in"
msgstr ""

#: src/login_screen/password_login.rs:335 src/login_screen/web_login.rs:113
msgid "Unknown error"
msgstr ""

#: src/login_screen/web_login.rs:111
msgid "API Limit Reached"
msgstr ""

#: src/login_screen/web_login.rs:112
msgid "Failed to log in"
msgstr ""

#: src/settings/dialog/app_page.rs:201 src/settings/dialog/app_page.rs:211
msgid "Failed to set setting 'keep running'"
msgstr ""

#: src/settings/dialog/app_page.rs:223
msgid "Failed to set setting 'sync on startup'"
msgstr ""

#: src/settings/dialog/app_page.rs:235
msgid "Failed to set setting 'sync on metered'"
msgstr ""

#: src/settings/dialog/app_page.rs:299
msgid "Failed to set setting 'limit articles duration'"
msgstr ""

#: src/settings/dialog/app_page.rs:425
msgid "Failed to set setting 'sync interval'"
msgstr ""

#: src/settings/dialog/keybinding_editor.rs:108
msgid "Disable Keybinding"
msgstr ""

#: src/settings/dialog/keybinding_editor.rs:125
msgid "Illegal Keybinding"
msgstr ""

#: src/settings/dialog/share_page.rs:111
msgid "Failed to set setting 'pocket enabled'"
msgstr ""

#: src/settings/dialog/share_page.rs:129
msgid "Failed to set setting 'instapaper enabled'"
msgstr ""

#: src/settings/dialog/share_page.rs:147
msgid "Failed to set setting 'twitter enabled'"
msgstr ""

#: src/settings/dialog/share_page.rs:165
msgid "Failed to set setting 'mastodon enabled'"
msgstr ""

#: src/settings/dialog/share_page.rs:183
msgid "Failed to set setting 'reddit enabled'"
msgstr ""

#: src/settings/dialog/share_page.rs:201
msgid "Failed to set setting 'telegram enabled'"
msgstr ""

#: src/settings/dialog/share_page.rs:225
msgid "Failed to set setting 'custom share enabled'"
msgstr ""

#: src/settings/dialog/share_page.rs:243
msgid "Failed to set setting 'custom share name'"
msgstr ""

#: src/settings/dialog/share_page.rs:260
msgid "Failed to set setting 'custom share url'"
msgstr ""

#: src/settings/dialog/shortcuts_page.rs:367
#: src/settings/dialog/shortcuts_page.rs:375
msgid "Failed to write keybinding"
msgstr ""

#: src/settings/dialog/views_page.rs:133
msgid "Newest First"
msgstr ""

#: src/settings/dialog/views_page.rs:134
msgid "Oldest First"
msgstr ""

#: src/settings/dialog/views_page.rs:240
msgid "Failed to set setting 'show relevant feeds'"
msgstr ""

#: src/settings/dialog/views_page.rs:261
msgid "Failed to set setting 'article order'"
msgstr ""

#: src/settings/dialog/views_page.rs:272 src/settings/dialog/views_page.rs:285
msgid "Failed to set setting 'show thumbnails'"
msgstr ""

#: src/settings/dialog/views_page.rs:318
msgid "Failed to set article theme"
msgstr ""

#: src/settings/dialog/views_page.rs:344
msgid "Failed to set setting 'allow article selection'"
msgstr ""

#: src/settings/dialog/views_page.rs:363
msgid "Failed to set setting 'content width'"
msgstr ""

#: src/settings/dialog/views_page.rs:381
msgid "Failed to set setting 'line height'"
msgstr ""

#: src/settings/dialog/views_page.rs:400
msgid "Failed to set setting 'article font'"
msgstr ""

#: src/settings/dialog/views_page.rs:427
msgid "Failed to set setting 'use system font'"
msgstr ""

#: src/settings/feed_list.rs:31
msgid "Alphabetical"
msgstr ""

#: src/settings/general.rs:46
msgid "15 Minutes"
msgstr ""

#: src/settings/general.rs:47
msgid "30 Minutes"
msgstr ""

#: src/settings/general.rs:48
msgid "1 Hour"
msgstr ""

#: src/settings/general.rs:49
msgid "2 Hours"
msgstr ""

#: src/settings/general.rs:102
msgid "Forever"
msgstr ""

#: src/settings/general.rs:103
msgid "One Year"
msgstr ""

#: src/settings/general.rs:104
msgid "6 Months"
msgstr ""

#: src/settings/general.rs:105
msgid "One Month"
msgstr ""

#: src/settings/general.rs:106
msgid "One Week"
msgstr ""

#: src/share/share_popover.rs:137
msgid "No article selected"
msgstr ""

#: src/share/share_popover.rs:144
msgid "Article does not have URL"
msgstr ""

#: src/share/share_popover.rs:152
msgid "Article shared with {}"
msgstr ""

#: src/sidebar/feed_list/item_row.rs:475 src/sidebar/tag_list/tag_row.rs:233
msgid "Set Read"
msgstr ""

#: src/sidebar/feed_list/item_row.rs:476 src/sidebar/tag_list/tag_row.rs:234
msgid "Edit"
msgstr ""

#: src/sidebar/feed_list/item_row.rs:477 src/sidebar/tag_list/tag_row.rs:235
msgid "Delete"
msgstr ""

#: src/util/date_util.rs:28
msgid "Today"
msgstr ""

#: src/util/date_util.rs:30
msgid "Yesterday"
msgstr ""
