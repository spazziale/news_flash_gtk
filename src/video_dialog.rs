use clapper::MediaItem;
use clapper_gtk::{SimpleControls, TitleHeader, Video};
use glib::subclass;
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, Widget};
use libadwaita::{subclass::prelude::*, Dialog};
use news_flash::models::Url;

use crate::app::App;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/media/video_dialog.blp")]
    pub struct VideoDialog {
        #[template_child]
        pub video: TemplateChild<Video>,
        #[template_child]
        pub title: TemplateChild<TitleHeader>,
        #[template_child]
        pub controls: TemplateChild<SimpleControls>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for VideoDialog {
        const NAME: &'static str = "VideoDialog";
        type ParentType = Dialog;
        type Type = super::VideoDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for VideoDialog {}

    impl WidgetImpl for VideoDialog {}

    impl AdwDialogImpl for VideoDialog {}
}

glib::wrapper! {
    pub struct VideoDialog(ObjectSubclass<imp::VideoDialog>)
        @extends Widget, Dialog;
}

impl VideoDialog {
    pub fn new_url(url: &Url) -> Option<Self> {
        let dialog = glib::Object::new::<Self>();
        let imp = dialog.imp();

        let item = MediaItem::new(url.as_str());
        let queue = imp.video.player().and_then(|player| player.queue());

        imp.video.connect_toggle_fullscreen(move |_video| {
            let window = App::default().main_window();
            window.set_fullscreened(!window.is_fullscreened());
        });

        if let Some(queue) = queue {
            queue.add_item(&item);
            queue.select_item(Some(&item));

            Some(dialog)
        } else {
            None
        }
    }
}
