mod feed_list;
mod loader;
pub mod models;
mod tag_list;

use crate::app::App;
use crate::util::GtkUtil;
pub use feed_list::models::{FeedListItemID, FeedListTree};
use feed_list::FeedList;
use glib::{clone, subclass, ControlFlow, SourceId};
use gtk4::{
    prelude::*, subclass::prelude::*, Box, CompositeTemplate, DropControllerMotion, EventControllerMotion,
    GestureClick, GestureDrag, Image, Label, ListBox, ListBoxRow, Revealer, ScrolledWindow, TickCallbackId, Widget,
};
pub use loader::SidebarLoader;
pub use models::SidebarIterateItem;
use models::SidebarSelection;
use news_flash::models::NEWSFLASH_TOPLEVEL;
use std::cell::{Cell, RefCell};
use std::rc::Rc;
use std::time::Duration;
pub use tag_list::models::TagListModel;
use tag_list::TagList;

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/sidebar/sidebar.blp")]
    pub struct SideBar {
        #[template_child]
        pub sidebar_scroll: TemplateChild<ScrolledWindow>,
        #[template_child]
        pub drop_motion: TemplateChild<DropControllerMotion>,
        #[template_child]
        pub drag_gesture: TemplateChild<GestureDrag>,

        #[template_child]
        pub subscriptions_event_box: TemplateChild<Box>,
        #[template_child]
        pub subscriptions_motion: TemplateChild<EventControllerMotion>,
        #[template_child]
        pub subscriptions_click: TemplateChild<GestureClick>,

        #[template_child]
        pub tags_event_box: TemplateChild<Box>,
        #[template_child]
        pub tags_motion: TemplateChild<EventControllerMotion>,
        #[template_child]
        pub tags_click: TemplateChild<GestureClick>,

        #[template_child]
        pub tags_box: TemplateChild<Box>,
        #[template_child]
        pub all_articles_fake_list: TemplateChild<ListBox>,
        #[template_child]
        pub all_articles_row: TemplateChild<ListBoxRow>,
        #[template_child]
        pub item_count_all_label: TemplateChild<Label>,
        #[template_child]
        pub feed_list: TemplateChild<FeedList>,
        #[template_child]
        pub tag_list: TemplateChild<TagList>,
        #[template_child]
        pub subscriptions_expander: TemplateChild<Image>,
        #[template_child]
        pub tags_expander: TemplateChild<Image>,
        #[template_child]
        pub subscriptions_revealer: TemplateChild<Revealer>,
        #[template_child]
        pub tags_revealer: TemplateChild<Revealer>,

        pub selection: Rc<RefCell<SidebarSelection>>,
        pub delayed_all_selection: RefCell<Option<SourceId>>,
        pub drag_auto_scroll: RefCell<Option<TickCallbackId>>,

        pub delay_next_feedlist_activation: Cell<bool>,
        pub delayed_feedlist_selection: RefCell<Option<SourceId>>,

        pub delay_next_taglist_activation: Cell<bool>,
        pub delayed_taglist_selection: RefCell<Option<SourceId>>,
    }

    impl Default for SideBar {
        fn default() -> Self {
            Self {
                sidebar_scroll: TemplateChild::default(),
                drop_motion: TemplateChild::default(),
                drag_gesture: TemplateChild::default(),

                subscriptions_event_box: TemplateChild::default(),
                subscriptions_motion: TemplateChild::default(),
                subscriptions_click: TemplateChild::default(),

                tags_event_box: TemplateChild::default(),
                tags_motion: TemplateChild::default(),
                tags_click: TemplateChild::default(),

                tags_box: TemplateChild::default(),
                all_articles_fake_list: TemplateChild::default(),
                all_articles_row: TemplateChild::default(),
                item_count_all_label: TemplateChild::default(),
                feed_list: TemplateChild::default(),
                tag_list: TemplateChild::default(),
                subscriptions_expander: TemplateChild::default(),
                tags_expander: TemplateChild::default(),
                subscriptions_revealer: TemplateChild::default(),
                tags_revealer: TemplateChild::default(),

                selection: Rc::new(RefCell::new(SidebarSelection::All)),
                delayed_all_selection: RefCell::new(None),
                drag_auto_scroll: RefCell::new(None),

                delay_next_feedlist_activation: Cell::new(false),
                delayed_feedlist_selection: RefCell::new(None),

                delay_next_taglist_activation: Cell::new(false),
                delayed_taglist_selection: RefCell::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SideBar {
        const NAME: &'static str = "SideBar";
        type ParentType = Box;
        type Type = super::SideBar;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SideBar {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for SideBar {}

    impl BoxImpl for SideBar {}
}

glib::wrapper! {
    pub struct SideBar(ObjectSubclass<imp::SideBar>)
        @extends Widget, Box;
}

impl Default for SideBar {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl SideBar {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();

        imp.feed_list.init(&imp.sidebar_scroll);

        self.all_articles_selection();
        self.feed_list_selection();
        self.tag_list_selection();
        self.setup_drag_autoscroll();

        Self::setup_expander(
            &imp.subscriptions_motion,
            &imp.subscriptions_click,
            &imp.subscriptions_event_box,
            &imp.subscriptions_expander,
            &imp.subscriptions_revealer,
        );
        Self::setup_expander(
            &imp.tags_motion,
            &imp.tags_click,
            &imp.tags_event_box,
            &imp.tags_expander,
            &imp.tags_revealer,
        );
    }

    fn all_articles_selection(&self) {
        let imp = self.imp();
        imp.all_articles_fake_list.connect_row_activated(
            clone!(@weak self as this => @default-panic, move |_list, _row| {
                let imp = this.imp();
                App::default().main_window().set_sidebar_selection((*imp.selection.borrow()).clone());
            }),
        );

        imp.all_articles_fake_list.connect_row_selected(
            clone!(@weak self as this => @default-panic, move |_all_fake_list, row|
            {
                // do nothing if selection was cleared
                if row.is_none() {
                    return;
                }

                let imp = this.imp();

                // deselect feed_list_handle & tag_list
                imp.feed_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);
                imp.tag_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);

                imp.selection.replace(SidebarSelection::All);
            }),
        );
    }

    fn feed_list_selection(&self) {
        let imp = self.imp();
        imp.feed_list.listview().connect_activate(
            clone!(@weak self as this => @default-panic, move |_listview, _pos| {
                let imp = this.imp();
                App::default().main_window().set_sidebar_selection((*imp.selection.borrow()).clone());
            }),
        );

        imp.feed_list.selection().connect_selection_changed(
            clone!(@weak self as this => @default-panic, move |selection_model, _pos, _n_items|
            {
                // do nothing if selection was cleared
                if selection_model.selected_item().is_none() {
                    return;
                }

                let imp = this.imp();

                // deselect 'all' & tag_list
                imp.all_articles_fake_list.unselect_all();
                imp.tag_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);

                if let Some((item, title, _item_count)) = imp.feed_list.get_selection() {
                    let selection = SidebarSelection::from_feed_list_selection(item, title);
                    imp.selection.replace(selection);
                }

                let selected_pos = selection_model.selected();

                // activate selected item
                // this emulates "single-click-activate" without the select on hover
                if imp.delay_next_feedlist_activation.get() {
                    GtkUtil::remove_source(imp.delayed_feedlist_selection.take());
                    imp.delayed_feedlist_selection.replace(Some(glib::timeout_add_local(
                        Duration::from_millis(300),
                        clone!(@weak this => @default-panic, move || {
                            let imp = this.imp();
                            imp.delayed_feedlist_selection.take();
                            imp.feed_list.listview().emit_by_name::<()>("activate", &[&selected_pos]);
                            ControlFlow::Break
                        }),
                    )));
                } else {
                    imp.feed_list.listview().emit_by_name::<()>("activate", &[&selected_pos]);
                }
            }),
        );
    }

    fn tag_list_selection(&self) {
        let imp = self.imp();
        imp.tag_list
            .listview()
            .connect_activate(clone!(@weak self as this => @default-panic, move |_listview, _pos| {
                let imp = this.imp();
                App::default().main_window().set_sidebar_selection((*imp.selection.borrow()).clone());
            }));

        imp.tag_list.selection().connect_selection_changed(
            clone!(@weak self as this => @default-panic, move |selection_model, _pos, _n_items|
            {
                // do nothing if selection was cleared
                if selection_model.selected_item().is_none() {
                    return;
                }

                let imp = this.imp();

                // deselect 'all' & feed_list
                imp.all_articles_fake_list.unselect_all();
                imp.feed_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);

                if let Some((item, title)) = imp.tag_list.get_selection() {
                    let selection = SidebarSelection::from_tag_list_selection(item, title);
                    imp.selection.replace(selection);
                }

                let selected_pos = selection_model.selected();

                // activate selected item
                // this emulates "single-click-activate" without the select on hover
                if imp.delay_next_taglist_activation.get() {
                    GtkUtil::remove_source(imp.delayed_taglist_selection.take());
                    imp.delayed_taglist_selection.replace(Some(glib::timeout_add_local(
                        Duration::from_millis(300),
                        clone!(@weak this => @default-panic, move || {
                            let imp = this.imp();
                            imp.delayed_taglist_selection.take();
                            imp.tag_list.listview().emit_by_name::<()>("activate", &[&selected_pos]);
                            ControlFlow::Break
                        }),
                    )));
                } else {
                    imp.tag_list.listview().emit_by_name::<()>("activate", &[&selected_pos]);
                }
            }),
        );
    }

    fn setup_drag_autoscroll(&self) {
        let imp = self.imp();
        imp.drop_motion.connect_motion(
            clone!(@weak self as this => @default-panic, move |_drag_motion, _x, y| {
                let imp = this.imp();
                if y < 60.0 {
                    if imp.drag_auto_scroll.borrow().is_none() {
                        imp.drag_auto_scroll.replace(Some(imp.sidebar_scroll.add_tick_callback(|scroll, _clock| {
                            let adjustment = scroll.vadjustment();
                            adjustment.set_value(adjustment.value() - 5.0);
                            ControlFlow::Continue
                        })));
                    }
                } else if y > this.height() as f64 - 60.0 {
                    if imp.drag_auto_scroll.borrow().is_none() {
                        imp.drag_auto_scroll.replace(Some(imp.sidebar_scroll.add_tick_callback(|scroll, _clock| {
                            let adjustment = scroll.vadjustment();
                            adjustment.set_value(adjustment.value() + 5.0);
                            ControlFlow::Continue
                        })));
                    }
                } else if let Some(source_id) = imp.drag_auto_scroll.take() {
                    source_id.remove();
                }
            }),
        );
        imp.drop_motion
            .connect_leave(clone!(@weak self as this => @default-panic, move |_drag_motion| {
                if let Some(source_id) = this.imp().drag_auto_scroll.take() {
                    source_id.remove();
                };
            }));
        imp.drag_gesture.connect_drag_end(
            clone!(@weak self as this => @default-panic, move |_gesture_drag, _x, _y| {
                if let Some(source_id) = this.imp().drag_auto_scroll.take() {
                    source_id.remove();
                };
            }),
        );
    }

    pub fn feed_list(&self) -> &FeedList {
        let imp = self.imp();
        &imp.feed_list
    }

    pub fn tag_list(&self) -> &TagList {
        let imp = self.imp();
        &imp.tag_list
    }

    pub fn restore_selection(&self, selection: SidebarSelection) {
        let imp = self.imp();

        match selection {
            SidebarSelection::All => {
                imp.feed_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);
                imp.tag_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);

                imp.all_articles_fake_list.select_row(Some(&*imp.all_articles_row));
            }
            SidebarSelection::FeedList(item_id, _name) => {
                imp.all_articles_fake_list.unselect_all();
                imp.tag_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);

                if let FeedListItemID::Feed(mapping) = &item_id {
                    if mapping.category_id != *NEWSFLASH_TOPLEVEL {
                        imp.feed_list.expand_category(&mapping.category_id);
                    }
                }

                imp.feed_list.set_selection(item_id);
            }
            SidebarSelection::Tag(tag_id, _name) => {
                imp.all_articles_fake_list.unselect_all();
                imp.feed_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);

                imp.tag_list.set_selection(tag_id);
            }
        }
    }

    pub fn update_feedlist(&self, tree: FeedListTree) {
        let imp = self.imp();
        imp.feed_list.update(tree);
    }

    pub fn update_taglist(&self, list: TagListModel) {
        let imp = self.imp();
        imp.tag_list.update(list);
    }

    pub fn hide_taglist(&self) {
        let imp = self.imp();
        imp.tags_box.set_visible(false);
    }

    pub fn show_taglist(&self) {
        let imp = self.imp();
        imp.tags_box.set_visible(true);
    }

    pub fn update_all(&self, item_count: i64) {
        let imp = self.imp();
        imp.item_count_all_label.set_text(&format!("{}", item_count));
        imp.item_count_all_label.set_visible(item_count > 0);
    }

    fn setup_expander(
        motion_event: &EventControllerMotion,
        click_gesture: &GestureClick,
        row: &Box,
        expander: &Image,
        revealer: &Revealer,
    ) {
        motion_event.connect_enter(clone!(@weak row => @default-panic, move |_controller, _x, _y| {
            row.add_css_class("highlight");
        }));
        motion_event.connect_leave(clone!(@weak row => @default-panic, move |_controller| {
            row.remove_css_class("highlight");
        }));

        click_gesture.connect_released(clone!(
            @weak expander,
            @weak revealer => @default-panic, move |_gesture, times, _x, _y|
        {
            if times != 1 {
                return
            }
            Self::toggle_expand_list(&revealer, &expander);
        }));
    }

    fn expand_list(revealer: &Revealer, expander: &Image) {
        if !revealer.is_child_revealed() {
            Self::toggle_expand_list(revealer, expander);
        }
    }

    fn toggle_expand_list(revealer: &Revealer, expander: &Image) {
        if revealer.is_child_revealed() {
            expander.add_css_class("backward-arrow-collapsed");
            expander.remove_css_class("backward-arrow-expanded");
            revealer.set_reveal_child(false);
        } else {
            expander.remove_css_class("backward-arrow-collapsed");
            expander.add_css_class("backward-arrow-expanded");
            revealer.set_reveal_child(true);
        }
    }

    pub fn select_all_button_no_update(&self) {
        let imp = self.imp();
        imp.selection.replace(SidebarSelection::All);
        GtkUtil::remove_source(imp.delayed_all_selection.take());
        imp.all_articles_fake_list.select_row(Some(&*imp.all_articles_row));
    }

    pub fn select_all_button(&self) {
        let imp = self.imp();

        imp.selection.replace(SidebarSelection::All);
        imp.all_articles_fake_list.select_row(Some(&*imp.all_articles_row));

        GtkUtil::remove_source(imp.delayed_all_selection.take());
        imp.delayed_all_selection.replace(Some(glib::timeout_add_local(
            Duration::from_millis(300),
            clone!(@weak self as this => @default-panic, move || {
                let imp = this.imp();
                App::default().main_window().set_sidebar_selection(SidebarSelection::All);
                imp.delayed_all_selection.take();
                ControlFlow::Break
            }),
        )));
    }

    pub fn select_next_item(&self) {
        let imp = self.imp();

        let select_next = match *imp.selection.borrow() {
            SidebarSelection::All => SidebarIterateItem::FeedListSelectFirstItem,
            SidebarSelection::FeedList(_, _) => imp.feed_list.calc_next_item(),
            SidebarSelection::Tag(_, _) => imp.tag_list.get_next_item(),
        };
        self.select_item(select_next)
    }

    pub fn select_prev_item(&self) {
        let imp = self.imp();

        let select_next = match *imp.selection.borrow() {
            SidebarSelection::All => SidebarIterateItem::TagListSelectLastItem,
            SidebarSelection::FeedList(_, _) => imp.feed_list.calc_prev_item(),
            SidebarSelection::Tag(_, _) => imp.tag_list.get_prev_item(),
        };
        self.select_item(select_next)
    }

    fn select_item(&self, selection: SidebarIterateItem) {
        let imp = self.imp();

        self.deselect();

        match selection {
            SidebarIterateItem::SelectAll => {
                self.select_all_button();
            }
            SidebarIterateItem::SelectFeedListItem(item_id) => {
                imp.delay_next_feedlist_activation.set(true);
                imp.feed_list.set_selection(item_id);
            }
            SidebarIterateItem::FeedListSelectFirstItem => {
                Self::expand_list(&imp.subscriptions_revealer, &imp.subscriptions_expander);
                if let Some(item) = imp.feed_list.get_first_item() {
                    imp.delay_next_feedlist_activation.set(true);
                    imp.feed_list.set_selection(item);
                }
            }
            SidebarIterateItem::FeedListSelectLastItem => {
                Self::expand_list(&imp.subscriptions_revealer, &imp.subscriptions_expander);
                if let Some(item) = imp.feed_list.get_last_item() {
                    imp.delay_next_feedlist_activation.set(true);
                    imp.feed_list.set_selection(item);
                }
            }
            SidebarIterateItem::SelectTagList(id) => {
                imp.delay_next_taglist_activation.set(true);
                imp.tag_list.set_selection(id);
            }
            SidebarIterateItem::TagListSelectFirstItem => {
                // if tags not supported or not available jump back to "All Articles"
                if !imp.tags_box.is_visible() {
                    return self.select_item(SidebarIterateItem::SelectAll);
                }
                Self::expand_list(&imp.tags_revealer, &imp.tags_expander);
                if let Some(item) = imp.tag_list.get_first_item() {
                    imp.delay_next_taglist_activation.set(true);
                    imp.tag_list.set_selection(item);
                }
            }
            SidebarIterateItem::TagListSelectLastItem => {
                // if tags not supported or not available jump back to "All Articles"
                if !imp.tags_box.is_visible() {
                    return self.select_item(SidebarIterateItem::FeedListSelectLastItem);
                }
                Self::expand_list(&imp.tags_revealer, &imp.tags_expander);
                if let Some(item) = imp.tag_list.get_last_item() {
                    imp.delay_next_taglist_activation.set(true);
                    imp.tag_list.set_selection(item);
                }
            }
            SidebarIterateItem::NothingSelected => {
                self.select_all_button();
            }
        }
    }

    pub fn get_selection(&self) -> SidebarSelection {
        let imp = self.imp();
        (*imp.selection.borrow()).clone()
    }

    fn deselect(&self) {
        let imp = self.imp();

        GtkUtil::remove_source(imp.delayed_all_selection.take());
        GtkUtil::remove_source(imp.delayed_feedlist_selection.take());
        GtkUtil::remove_source(imp.delayed_taglist_selection.take());
        imp.all_articles_fake_list.unselect_all();
        imp.feed_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);
        imp.tag_list.selection().set_selected(gtk4::INVALID_LIST_POSITION);
    }

    pub fn expand_collapse_selected_category(&self) {
        let imp = self.imp();
        imp.feed_list.expand_collapse_selected_category()
    }

    pub fn get_subscriptions_expanded(&self) -> bool {
        self.imp().subscriptions_revealer.is_child_revealed()
    }

    pub fn set_subscriptions_expanded(&self, expanded: bool) {
        let imp = self.imp();

        if imp.subscriptions_revealer.is_child_revealed() != expanded {
            Self::toggle_expand_list(&imp.subscriptions_revealer.get(), &imp.subscriptions_expander.get());
        }
    }

    pub fn get_tags_expanded(&self) -> bool {
        self.imp().tags_revealer.is_child_revealed()
    }

    pub fn set_tags_expanded(&self, expanded: bool) {
        let imp = self.imp();

        if imp.tags_revealer.is_child_revealed() != expanded {
            Self::toggle_expand_list(&imp.tags_revealer.get(), &imp.tags_expander.get());
        }
    }
}
